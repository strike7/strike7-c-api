# pragma once

/**
* This is provided as is.
*
* This provides defines and a struct to hold the interface functions that
* will be used by the applet class to send commands to the api.
*
* Author: Nicholas Welters
* Data  : 01/06/2014
*/

# include <cstdint>
# include <Windows.h>

namespace Strike7
{
	/////////////////////////
	// API document types //
	///////////////////////
	typedef uint16_t AppletID_t;
	typedef void ImageBGRA;

	////////////////////////
	// API dll functions //
	//////////////////////
	// event callbacks
	typedef void ( __stdcall *AppletActiveEvent   )( AppletID_t appletID, uint16_t endPointID, uint32_t endPointType );
	typedef void ( __stdcall *AppletInactiveEvent )( AppletID_t appletID, uint16_t endPointID );
	typedef void ( __stdcall *AppletTouchEvent    )( AppletID_t appletID, uint16_t endPointID, uint32_t x, uint32_t y, bool pressed );
	typedef void ( __stdcall *AppletResetEvent    )( AppletID_t appletID, uint16_t endPointID );

	typedef __declspec( dllimport ) void ( *RegisterAppletActiveEvent   )( AppletActiveEvent  , AppletID_t appletID );
	typedef __declspec( dllimport ) void ( *RegisterAppletInactiveEvent )( AppletInactiveEvent, AppletID_t appletID );
	typedef __declspec( dllimport ) void ( *RegisterAppletTouchEvent    )( AppletTouchEvent   , AppletID_t appletID );
	typedef __declspec( dllimport ) void ( *RegisterAppletResetEvent    )( AppletResetEvent   , AppletID_t appletID );

	typedef __declspec( dllimport ) AppletID_t( *OpenApplet  )( const TCHAR * name, uint32_t iconWidth, uint32_t iconHeight, ImageBGRA * iconBGRA , uint32_t supportedDevices[ ] );
	typedef __declspec( dllimport ) void      ( *CloseApplet )( AppletID_t appletID );

	typedef __declspec( dllimport ) void( *AppletUpdateScreen )( AppletID_t appletID, uint16_t endPointID );
	typedef __declspec( dllimport ) void( *AppletScroll       )( AppletID_t appletID, uint16_t endPointID, uint32_t x, uint32_t y, uint32_t w, uint32_t h, int32_t dx, int32_t dy );
	typedef __declspec( dllimport ) void( *AppletDraw         )( AppletID_t appletID, uint16_t endPointID,  int32_t x,  int32_t y, uint32_t w, uint32_t h, ImageBGRA * data );
	typedef __declspec( dllimport ) void( *AppletFill         )( AppletID_t appletID, uint16_t endPointID,  int32_t x,  int32_t y, uint32_t w, uint32_t h, uint32_t colour );
	typedef __declspec( dllimport ) void( *AppletDrawText     )( AppletID_t appletID, uint16_t endPointID,  int32_t x,  int32_t y, uint32_t size, uint32_t colour, const TCHAR* text );

	///////////////////
	//API Functions //
	/////////////////
	class Strike7API
	{
		public:
			Strike7API( );
			~Strike7API( );

			CloseApplet closeApplet;

			AppletUpdateScreen appletUpdateScreen;
			AppletScroll       appletScroll;
			AppletDraw         appletDraw;
			AppletFill         appletFill;
			AppletDrawText     appletDrawText;
	};
}

// unsigned char buffer[HEIGHT][WIDTH][4];