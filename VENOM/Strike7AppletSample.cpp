# include "Strike7AppletSample.hpp"


namespace Strike7
{
	Strike7AppletSample::Strike7AppletSample( const std::wstring & name, const Strike7ImagePtr & icon )
		: Strike7Applet( name, icon )
		, lastX( 0 )
		, lastY( 0 )
	{ }


	Strike7AppletSample::~Strike7AppletSample( )
	{ }


	////////////////
	// Callbacks //
	//////////////
	void Strike7AppletSample::appletActiveEvent( uint16_t endPointID, uint16_t endPointType )
	{
		std::wstringstream ss1;
		ss1 << "ID " << getID( ) << " < EndPoint [ ID " << endPointID << " ][ Type " << endPointType << " ] >";

		std::wstringstream ss2;
		ss2 << std::setfill( L'0' ) << std::setw( 3 ) << lastX << " ";
		ss2 << std::setfill( L'0' ) << std::setw( 3 ) << lastY;

		AppletDrawText( endPointID, 10, 10, 32, 0xff0000, L"Applet Activated" );
		AppletDrawText( endPointID, 10, 46, 20, 0x0000ff, ss1.str( ).c_str( ) );
		AppletDrawText( endPointID, 10, 96, 32, 0x00ff00, L"Touch ___ ___ X" );
		AppletDrawText( endPointID, 105, 96, 32, 0x00ff00, ss2.str( ).c_str( ) );
		AppletUpdateScreen( endPointID );
	}

	void Strike7AppletSample::appletInactiveEvent( uint16_t endPointID )
	{
	
	}

	void Strike7AppletSample::appletTouchEvent( uint16_t endPointID, uint32_t x, uint32_t y, bool pressed )
	{
		lastX = x;
		lastY = y;

		std::wstringstream ss;
		ss << std::setfill( L'0' ) << std::setw( 3 ) << lastX << " ";
		ss << std::setfill( L'0' ) << std::setw( 3 ) << lastY << " ";
		ss << ( pressed ? "O" : "X" );

		AppletFill( endPointID, 105, 98, 150, 34, 0x000000 );
		AppletDrawText( endPointID, 105, 96, 32, 0x00ff00, ss.str( ).c_str( ) );
		AppletUpdateScreen( endPointID );
	}

	void Strike7AppletSample::appletResetEvent( uint16_t endPointID )
	{
	
	}
}
