#pragma once

/**
 * This is provided as is.
 *
 * A light wrapper to free images api for easer image manipulation
 * and interaction with the strike 7 api.
 *
 *
 * Author: Nicholas Welters
 * Data  : 01/06/2014
 */

# include "Strike7API.hpp"

# include <FreeImage.h>

# include <string>
# include <memory>

namespace Strike7
{
	class Strike7Image
	{
		private:
			FIBITMAP * pImage;
			uint32_t width;
			uint32_t height;

		public:
			Strike7Image( FREE_IMAGE_FORMAT FIF, std::string fileLocation, int flags = 0 );
			~Strike7Image( );

			uint32_t getWidth( );
			uint32_t getHeight( );

			BYTE * getBits( );
	};

	typedef std::shared_ptr< Strike7Image > Strike7ImagePtr;
}

