#pragma once

/**
 * This is provided as is.
 *
 * This just does some very basic interactions with the strike 7 api
 * mostly to prove that the whole thing works.
 *
 * Author: Nicholas Welters
 * Data  : 01/06/2014
 */

#include "Strike7Applet.hpp"

namespace Strike7
{
	class Strike7AppletSample: public Strike7Applet
	{
		private:
			uint32_t lastX;
			uint32_t lastY;

		public:
			Strike7AppletSample( const std::wstring & name, const Strike7ImagePtr & icon );
			virtual ~Strike7AppletSample( );

			////////////////
			// CallBacks //
			//////////////
			virtual void appletActiveEvent( uint16_t endPointID, uint16_t endPointType );
			virtual void appletInactiveEvent( uint16_t endPointID );
			virtual void appletTouchEvent( uint16_t endPointID, uint32_t x, uint32_t y, bool pressed );
			virtual void appletResetEvent( uint16_t endPointID );
	};
}
