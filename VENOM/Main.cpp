# pragma once

/**
 * This is provided as is.
 *
 * This is the entry point to the VENOM mod API test application
 *
 *
 * Author: Nicholas Welter
 * Data  : 01/06/2014
 */

# include "Strike7dll.hpp"
# include "Strike7AppletSample.hpp"

# include <iostream>

int main( )
{
	std::cout << "/////////////////\n// VENOM TEST //\n///////////////\n\n";
	
	////////////////////////
	// Load applet icons //
	//////////////////////
	Strike7::Strike7ImagePtr pIcon1( new Strike7::Strike7Image( FIF_ICO, "Data\\Test.ico" ) );
	Strike7::Strike7ImagePtr pIcon2( new Strike7::Strike7Image( FIF_PNG, "Data\\MadCatzIcon.png" ) );

	///////////////////////
	// Load the API dll //
	/////////////////////
	Strike7::Strike7dll strike7dll( "Strike7API.dll" );

	///////////////////////
	// Load the applets //
	/////////////////////
	std::shared_ptr< Strike7::Strike7Applet > pApplet1( new Strike7::Strike7AppletSample( L"TestApplet_1", pIcon1 ) );
	std::shared_ptr< Strike7::Strike7Applet > pApplet2( new Strike7::Strike7AppletSample( L"TestApplet_2", pIcon2 ) );

	////////////////////////////////////////////
	// Register the applets with the api dll //
	//////////////////////////////////////////
	strike7dll.registerApplet( pApplet1 );
	strike7dll.registerApplet( pApplet2 );

	std::cout << "\n";
	system( "pause" );
	
	return 0;
}
