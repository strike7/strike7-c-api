# include "Strike7Applet.hpp"

namespace Strike7
{
	Strike7Applet::Strike7Applet( const std::wstring & name, const Strike7ImagePtr & icon )
		: name( name )
		, icon( icon )
		, appletID( 0 )
	{

	}

	Strike7Applet::~Strike7Applet( )
	{

	}

	/**
	 * @return The applet name.
	 */
	std::wstring & Strike7Applet::getName( )
	{
		return name;
	}

	/**
	 * @return The applet icon.
	 */
	Strike7ImagePtr & Strike7Applet::getIcon( )
	{
		return icon;
	}

	/**
	 * @return The applet ID.
	 */
	AppletID_t Strike7Applet::getID( )
	{
		return appletID;
	}

	/**
	* @param appletID The applet ID.
	* @param apiFunctions The api functions need to send commands to the api.
	*/
	void Strike7Applet::initialise( const AppletID_t & appletID, const Strike7API & apiFunctions )
	{
		this->appletID = appletID;
		this->apiFunctions = apiFunctions;
	}

	///////////////
	// Calls to //
	/////////////
	void Strike7Applet::AppletUpdateScreen( uint16_t endPointID  )
	{
		apiFunctions.appletUpdateScreen( appletID, endPointID );
	}

	void Strike7Applet::AppletScroll( uint16_t endPointID, uint32_t x, uint32_t y, uint32_t w, uint32_t h, int32_t dx, int32_t dy )
	{
		apiFunctions.appletScroll( appletID, endPointID, x, y, w, h, dx, dy );
	}

	void Strike7Applet::AppletDraw( uint16_t endPointID, int32_t x, int32_t y, uint32_t w, uint32_t h, ImageBGRA * data )
	{
		apiFunctions.appletDraw( appletID, endPointID, x, y, w, h, data );
	}

	void Strike7Applet::AppletFill( uint16_t endPointID, int32_t x, int32_t y, uint32_t w, uint32_t h, uint32_t colour )
	{
		apiFunctions.appletFill( appletID, endPointID, x, y, w, h, colour );
	}

	void Strike7Applet::AppletDrawText( uint16_t endPointID, int32_t x, int32_t y, uint32_t size, uint32_t colour, const TCHAR* text )
	{
		apiFunctions.appletDrawText( appletID, endPointID, x, y, size, colour, text );
	}
}
